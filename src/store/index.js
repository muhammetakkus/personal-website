import { createStore } from 'vuex'

export default createStore({
  state: {
    lang: 'tr',
    theme: 'dark'
  },
  mutations: {
    changeLang (state, payload) {
      // mutate state
      state.lang = payload
    },
    changeTheme (state, payload) {
      // mutate state
      state.theme = payload
    }
  },
  actions: {
  },
  modules: {
  }
})

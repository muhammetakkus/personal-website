export default {
    data() {
        return {
            tr: {
                menu: ['AÇIK TEMA', 'EN', 'İLETİŞİM'],
                text: `<p>Merhaba, ben Muhammet Akkuş. Gelişime açık Mobil Uyumu Yüksek <span class="stress-it">Web Uygulamaları</span> yazıyorum. Bir sonraki projenizde beraber çalışmak için iletişime geçebilirsiniz.</p>`,
                label: {
                    textarea: 'Mesajınız',
                    input: 'Size ulaşabilmek için'
                },
                textarea_placeholder: 'Mesajınız veya herhangi bir proje..',
                contact_input_placeholder: 'GSM veya E-mail adresiniz..',
                send_button_text: 'GÖNDER'
            }
        }
    }
}
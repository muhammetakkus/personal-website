export default {
    data() {
        return {
            en: {
                menu: ['LIGHT THEME', 'TR', 'CONTACT'],
                text: `<p>Hi, I am Muhammet Akkuş! I'm a Web App Developer and I'm developing some progressive <span class="stress-it">Web Apps</span>. You can contact with me for your next project to work together.</p>`,
                label: {
                    textarea: 'Your message',
                    input: 'To contact with you'
                },
                textarea_placeholder: 'Your messages.. Feel free..',
                contact_input_placeholder: 'Your contact info..',
                send_button_text: 'SEND PLEASE'
            }
        }
    }
}